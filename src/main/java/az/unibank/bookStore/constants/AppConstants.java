package az.unibank.bookStore.constants;

import lombok.AccessLevel;
import lombok.NoArgsConstructor;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public final class AppConstants {

    public static final String DATE_TIME_FORMAT = "dd.MM.yyyy HH:mm:ss";
}
