package az.unibank.bookStore.mapper;

import az.unibank.bookStore.dto.AuthorDto;
import az.unibank.bookStore.dto.BookDto;
import az.unibank.bookStore.dto.UserDto;
import az.unibank.bookStore.dto.UserTypeDto;
import az.unibank.bookStore.entity.Book;
import az.unibank.bookStore.entity.User;
import az.unibank.bookStore.entity.UserType;

public class Converter {
    public static BookDto convertFromBook(Book b) {
        BookDto bookDto = new BookDto();
        bookDto.setId(b.getId());
        bookDto.setDescription(b.getDescription());
        bookDto.setInsertDate(b.getInsertDate());
        bookDto.setName(b.getName());
        bookDto.setStatus(b.getStatus());
//        bookDto.setAuthor(convertFromAuthor(b.getAuthor()));
//        bookDto.setUser(convertFromUser(b.getUser()));
        return bookDto;
    }

    private static UserDto convertFromUser(User u) {
        UserDto d = new UserDto();
        d.setId(u.getId());
        d.setName(u.getName());
        d.setStatus(u.getStatus());
        d.setSurname(u.getSurname());
        d.setInsertDate(u.getInsertDate());
        d.setEmail(u.getEmail());
        d.setUsername(u.getUsername());
        d.setUserType(convertFromUserType(u.getUserType()));
        return d;
    }

    private static UserTypeDto convertFromUserType(UserType u) {
        UserTypeDto d = new UserTypeDto();
        d.setId(u.getId());
        d.setName(u.getName());
        d.setStatus(u.getStatus());
        d.setInsertDate(u.getInsertDate());
        return d;

    }

//    private static AuthorDto convertFromAuthor(Author a) {
//        AuthorDto d = new AuthorDto();
//        d.setId(a.getId());
//        d.setDescription(a.getDescription());
//        d.setInsertDate(a.getInsertDate());
//        d.setName(a.getName());
//        d.setStatus(a.getStatus());
//        d.setSurname(a.getSurname());
//        return d;
//    }
}
