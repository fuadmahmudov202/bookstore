package az.unibank.bookStore.entity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.time.LocalDateTime;

@Data
@Entity
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Table(name = "USER_TYPE")
public class UserType {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID", insertable = false)
    private Long id;

    @Column(name = "NAME")
    private String name;

    @Column(name = "STATUS")
    private short status;

    @Column(name = "INSERTDATE")
    private LocalDateTime insertDate;

    public UserType(Long id,String name){
        this.id=id;
        this.name=name;
        this.insertDate=LocalDateTime.now();
        this.status=(short) 1;
    }
}
