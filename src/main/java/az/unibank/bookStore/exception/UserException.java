package az.unibank.bookStore.exception;

import az.unibank.bookStore.enums.ResultCode;

public class UserException extends BaseException{

    public UserException(ResultCode result) {
        super(result);
    }
    public static UserException userAlreadyExist() {
        return new UserException(ResultCode.USER_ALREADY_EXIST);
    }
    public static UserException authorNotFound() {
        return new UserException(ResultCode.AUTHOR_NOT_FOUND);
    }

}
