package az.unibank.bookStore.enums;

import java.util.HashMap;
import java.util.Map;

public enum UserType {
    STANDARD_USER(1L),
    AUTHOR(2L),
    PUBLISHER(3L);

    private final Long value;

    public static final Map<Long,UserType> VALUE_MAP= new HashMap<>();
    static {
        for (UserType status:values()) {
            VALUE_MAP.put(status.value,status);
        }
    }

    private UserType(Long value) {
        this.value = value;
    }
    public Long getValue() {
        return value;
    }

    public static UserType getStatus(Integer status){
        return VALUE_MAP.get(status);
    }
}
