package az.unibank.bookStore.service.impl;

import az.unibank.bookStore.dto.UserDto;
import az.unibank.bookStore.entity.User;
import az.unibank.bookStore.entity.UserType;
import az.unibank.bookStore.enums.ResultCode;
import az.unibank.bookStore.exception.UserException;
import az.unibank.bookStore.exception.UserTypeException;
import az.unibank.bookStore.repository.UserRepo;
import az.unibank.bookStore.repository.UserTypeRepo;
import az.unibank.bookStore.req.UserReq;
import az.unibank.bookStore.service.UserService;
import lombok.RequiredArgsConstructor;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import javax.security.auth.login.LoginException;
import java.time.LocalDateTime;

@Service
@RequiredArgsConstructor
public class UserServiceImpl implements UserService {
    private final UserRepo userRepo;
    private final UserTypeRepo userTypeRepo;
    private final BCryptPasswordEncoder bCryptPasswordEncoder;


    public static String currentUser() {
         SecurityContext context= SecurityContextHolder.getContext();
        return SecurityContextHolder.getContext()
                .getAuthentication()
                .getName();
    }

    @Override
    public UserDto saveUser(UserReq userReq) {
        UserType userType= userTypeRepo.findByName(userReq.getUserType())
//                .stream()
//                .findFirst()
                .orElseThrow(UserTypeException::userTypeNotFound);
        boolean isUsernameExist= userRepo.findByUsernameAndStatus(userReq.getUsername(),(short) 1).isPresent();
        if (isUsernameExist)
            throw new UserException(ResultCode.USER_ALREADY_EXIST);
        User user = mapToEntity(userReq,userType);
        return mapToDto(userRepo.save(user));
    }

    private User mapToEntity(UserReq userReq, UserType userType) {
        return User.builder().userType(userType)
                .email(userReq.getEmail())
                .password(bCryptPasswordEncoder.encode(userReq.getPassword()))
                .insertDate(LocalDateTime.now())
                .username(userReq.getUsername())
                .surname(userReq.getSurname())
                .name(userReq.getName())
                .status((short) 1).build();
    }

    private UserDto mapToDto(User user){
        return UserDto.builder().id(user.getId())
                .email(user.getEmail())
                .name(user.getName())
                .surname(user.getSurname())
                .username(user.getUsername())
                .insertDate(user.getInsertDate())
                .build();
    }
}
