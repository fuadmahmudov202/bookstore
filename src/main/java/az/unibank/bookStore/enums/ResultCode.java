package az.unibank.bookStore.enums;

import lombok.Getter;
import lombok.RequiredArgsConstructor;

@Getter
@RequiredArgsConstructor
public enum ResultCode {
    OK(1, "success"),
    BOOK_NOT_FOUND(-1, "book not found"),
    PUBLISHER_NOT_FOUND(-1, "publisher not found"),
    AUTHOR_NOT_FOUND(-1, "author not found"),

    USERTYPE_NOT_FOUND(-1, "usertype not found"),
    USER_ALREADY_EXIST(-1, "user already exist"),
    URL_NOT_FOUND(-1, "url not found"),
    USER_NOT_FOUND(-1, "user name or password incorrect "),

    INVALID_FIELD_VALUE(-3, "invalid field value"),
    INVALID_CREDENTIALS(-3, "authentication required"),


    REQUEST_METHOD_NOT_SUPPORTED(-9, "request method not supported"),
    SYSTEM_ERROR(-10, "system error");

    private final int code;
    private final String value;


}
