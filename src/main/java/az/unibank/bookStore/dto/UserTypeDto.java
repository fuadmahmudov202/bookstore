package az.unibank.bookStore.dto;

import lombok.Data;

import java.time.LocalDateTime;

@Data
public class UserTypeDto {
    private Long id;
    private String name;
    private short status;
    private LocalDateTime insertDate;
}
