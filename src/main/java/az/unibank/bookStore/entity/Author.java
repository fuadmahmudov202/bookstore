//package az.unibank.bookStore.entity;
//
//import lombok.AllArgsConstructor;
//import lombok.Builder;
//import lombok.Data;
//import lombok.NoArgsConstructor;
//
//import javax.persistence.*;
//import java.time.LocalDateTime;
//
//@Data
//@Entity
//@NoArgsConstructor
//@AllArgsConstructor
//@Builder
//@Table(name = "AUTHOR")
//
//public class Author {
//    @Id
//    @GeneratedValue(strategy = GenerationType.IDENTITY)
//    @Column(name = "ID", insertable = false)
//    private Long id;
//
//    @Column(name = "NAME")
//    private String name;
//
//    @Column(name = "SURNAME")
//    private String surname;
//
//    @Column(name = "DESCRIPTION")
//    private String description;
//
//    @Column(name = "STATUS")
//    private short status;
//
//    @Column(name = "INSERTDATE")
//    private LocalDateTime insertDate;
//}
