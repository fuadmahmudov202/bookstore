package az.unibank.bookStore.service;

import az.unibank.bookStore.dto.BookDto;
import az.unibank.bookStore.dto.BookResponse;
import az.unibank.bookStore.req.BookReq;

import java.util.List;

public interface BookService {
//    BookDto getAllBooks();

    BookResponse findAll();
    BookDto findById(Long id);

    List<BookDto> findByName(String name,Integer limit,Integer offset);

    BookResponse findByPublisherName(String name);

    BookDto saveBook(BookReq bookReq);

    BookDto update(Long id,BookReq bookReq);

    BookDto delete(Long id);

}
