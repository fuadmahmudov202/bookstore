package az.unibank.bookStore.service;

import az.unibank.bookStore.dto.UserDto;
import az.unibank.bookStore.req.UserReq;

public interface UserService {
    UserDto saveUser(UserReq userReq);
}
