package az.unibank.bookStore.repository;

import az.unibank.bookStore.entity.UserType;
import org.springframework.data.repository.CrudRepository;

import java.util.List;
import java.util.Optional;

public interface UserTypeRepo extends CrudRepository<UserType,Long> {

    List<UserType> findByNameStartsWith(String name);

    Optional<UserType> findByName(String name);
}
