package az.unibank.bookStore.req;

import az.unibank.bookStore.entity.User;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotBlank;

@NoArgsConstructor
@AllArgsConstructor
@Data
public class BookReq {
    @NotBlank(message = "name can not be null")
    private String name;
    private String description;
    @NotBlank(message = "publisher can not be null")
    private String  publisherName;
    @NotBlank(message = "author can not be null")
    private String authorName;
}
