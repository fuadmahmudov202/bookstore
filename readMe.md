# Book Store Application

It's an application which u can Trace your Book store.
You can use this [link]{http://localhost:8080/swagger-ui.html} or using swagger<br>
For authentication please use [username]{fuad} [password]{1234} <br>
Only saveUser service working without authentication.Please use basic authentication for other services<br>
For adding new User to application u can use this service [url]{http://localhost:8080/api/user/save
}<br>
Request body should be like this<br>
{<br>
"email": "string",<br>
"name": "string",<br>
"password": "string",<br>
"surname": "string",<br>
"userType": "string",<br>
"username": "string"<br>
}<br>
<br>
<br>
<br>
For list All book use /Books service<br>
Responce type is
{<br>
{<br>
"bookDto": [<br>
{<br>
"authorName": "string",<br>
"description": "string",<br>
"id": 0,<br>
"insertDate": "2022-04-23T19:50:06.641Z",<br>
"name": "string",<br>
"publisherName": "string",<br>
"status": 0<br>
}
]}<br>

You can update any book with [url]{http://localhost:8080/api/books/1}
<br>
and You should Use respose body like this<br>
{<br>
<br>"authorName": "string",
<br>"description": "string",
<br>"name": "string",
<br>"publisherName": "string"
<br>}
<br>

You can save new Book with [url] {http://localhost:8080/api/books/save}
And Your respose body should be like this<br>
{<br>
<br>"authorName": "string",
<br>"description": "string",
<br>"name": "string",
<br>"publisherName": "string"
<br>}

You can delete any book with Using this service [url]{http://localhost:8080/api/books/delete/2
}
Respose Body is like this:<br>
{<br>
"id": 2,<br>
"name": "test",<br>
"status": 0,<br>
"description": "test book",<br>
"insertDate": "2022-04-22T00:20:25",<br>
"publisherName": "fuad",<br>
"authorName": "fuad"<br>
}<br>

You can search book with author name using this service [url]{http://localhost:8080/api/books/publisher/fuad}
<br>
Response body is like this<br>
{<br>
"bookDto": [<br>
{<br>
"authorName": "string",<br>
"description": "string",<br>
"id": 0,<br>
"insertDate": "2022-04-23T19:58:45.221Z",<br>
"name": "string",<br>
"publisherName": "string",<br>
"status": 0<br>
}<br>]}
<br>
<br><br><br>

My application using MySql DataBase actually 
i would like to use H2 database for more comfortable user experience.
but i got some trouble with H2 Database that's why i don't want to waste more time.
I upload MySql data exported file to WeTrasfer here u got link for download data [url]{https://we.tl/t-bYG22GHhKG }
<br>
If you face any problem with downloading Database please contact with me via email:<br>
fuadmahmudov202@gmail.com