package az.unibank.bookStore.exception;

import az.unibank.bookStore.enums.ResultCode;
import az.unibank.bookStore.model.ApiResult;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.security.authentication.InsufficientAuthenticationException;
import org.springframework.validation.FieldError;
import org.springframework.web.HttpRequestMethodNotSupportedException;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.servlet.NoHandlerFoundException;

import java.util.Optional;


@RestControllerAdvice
@Slf4j
public class GlobalExceptionHandler {

    @ResponseStatus(HttpStatus.NOT_FOUND)
    @ExceptionHandler(BaseException.class)
    public ApiResult baseException(BaseException exception) {
        log.warn("file exception : code {}, message {} ",
                exception.getResult().getCode(),
                exception.getResult().getValue());
        return new ApiResult(exception.getResult());
    }

    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ExceptionHandler(MethodArgumentNotValidException.class)
    public ApiResult invalidArgument(MethodArgumentNotValidException exception){
        log.warn("MethodArgumentNotValidException happened");

        Optional<String> message=exception.getBindingResult().getFieldErrors().stream()
                .map(FieldError::getDefaultMessage)
                .findFirst();

        return message.map(s -> new ApiResult(ResultCode.INVALID_FIELD_VALUE.getCode(), s))
                .orElseGet(() -> new ApiResult(ResultCode.INVALID_FIELD_VALUE));
    }

    @ResponseStatus(HttpStatus.NOT_FOUND)
    @ExceptionHandler(NoHandlerFoundException.class)
    public ApiResult urlNotFound(NoHandlerFoundException exception){
        log.warn("urlNotFound exception happened");
        return new ApiResult(ResultCode.URL_NOT_FOUND);
    }

    @ResponseStatus(HttpStatus.UNAUTHORIZED)
    @ExceptionHandler(InsufficientAuthenticationException.class)
    public ApiResult unauthorized(InsufficientAuthenticationException exception) {
        log.warn("insufficient authentication exception happened");
        return new ApiResult(ResultCode.INVALID_CREDENTIALS);
    }

    @ResponseStatus(HttpStatus.METHOD_NOT_ALLOWED)
    @ExceptionHandler(HttpRequestMethodNotSupportedException.class)
    public ApiResult methodNotSupported(HttpRequestMethodNotSupportedException exception){
        log.warn("methodNotSupported exception happened");
        return new ApiResult(ResultCode.REQUEST_METHOD_NOT_SUPPORTED);
    }


    @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
    @ExceptionHandler(Exception.class)
    public ApiResult unHandledExceptions(Exception exception) {
        return new ApiResult(ResultCode.SYSTEM_ERROR);
    }

}
