package az.unibank.bookStore.controller;

import az.unibank.bookStore.dto.BookResponse;
import az.unibank.bookStore.dto.UserDto;
import az.unibank.bookStore.req.UserReq;
import az.unibank.bookStore.service.UserService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@Slf4j
@RestController
@RequiredArgsConstructor
@RequestMapping("/user")
public class UserController {
    private final UserService userService;

    @PostMapping("/save")
    public UserDto saveNewUser(@Valid @RequestBody UserReq userReq) {
        log.info("endpoint called: /user/save  , method: GET");
            return userService.saveUser(userReq);
    }
}
