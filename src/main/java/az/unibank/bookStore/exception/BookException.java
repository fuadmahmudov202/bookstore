package az.unibank.bookStore.exception;

import az.unibank.bookStore.enums.ResultCode;

public class BookException extends BaseException {


    public BookException(ResultCode result) {
        super(result);
    }

    public static BookException bookNotFound() {
        return new BookException(ResultCode.BOOK_NOT_FOUND);
    }

}
