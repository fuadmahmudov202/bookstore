
package az.unibank.bookStore.exception;

import az.unibank.bookStore.enums.ResultCode;
import lombok.Getter;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
@Getter
public class BaseException extends RuntimeException {
    private final ResultCode result;
}
