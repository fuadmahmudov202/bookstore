package az.unibank.bookStore.mapper;

import az.unibank.bookStore.dto.BookDto;
import az.unibank.bookStore.entity.Book;
import org.mapstruct.Mapper;
import org.mapstruct.ReportingPolicy;

@org.mapstruct.Mapper(unmappedTargetPolicy = ReportingPolicy.IGNORE, componentModel = "spring")
public interface BookMapper {
    BookDto mapToDto(Book book);
}
