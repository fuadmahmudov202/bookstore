package az.unibank.bookStore.exception;

import az.unibank.bookStore.enums.ResultCode;

public class LoginException extends BaseException {

    public LoginException(ResultCode resultCode) {
        super(resultCode);
    }

    public static LoginException userNotFound() {
        return new LoginException(ResultCode.USER_NOT_FOUND);
    }
}
