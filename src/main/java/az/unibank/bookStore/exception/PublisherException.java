package az.unibank.bookStore.exception;

import az.unibank.bookStore.enums.ResultCode;

public class PublisherException extends BaseException{
    public PublisherException(ResultCode result) {
        super(result);
    }

    public static PublisherException publisherNotFound() {
        return new PublisherException(ResultCode.PUBLISHER_NOT_FOUND);
    }


}
