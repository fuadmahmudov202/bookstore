package az.unibank.bookStore.service.impl;

import az.unibank.bookStore.dto.BookResponse;
import az.unibank.bookStore.entity.Book;
import az.unibank.bookStore.dto.BookDto;
import az.unibank.bookStore.entity.User;
import az.unibank.bookStore.enums.UserType;
import az.unibank.bookStore.exception.BookException;
import az.unibank.bookStore.exception.PublisherException;
import az.unibank.bookStore.exception.UserException;
import az.unibank.bookStore.mapper.Converter;
import az.unibank.bookStore.repository.BookRepo;
import az.unibank.bookStore.repository.UserRepo;
import az.unibank.bookStore.req.BookReq;
import az.unibank.bookStore.service.BookService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
@Slf4j
public class BookServiceImpl implements BookService {
    private final BookRepo bookRepo;
    private final UserRepo userRepo;


//    @Override
//    public BookDto getAllBooks() {
//        bookRepo.findAll();
//    }

    @Override
    public BookResponse findAll() {
        List<BookDto> bookDtos= bookRepo.findAll().stream()
                .map(this::convertToDto)
                .collect(Collectors.toList());
        return BookResponse.builder()
                .bookDto(bookDtos)
                .build();
    }


    @Override
    public BookDto findById(Long id) {
        Optional<Book> book = bookRepo.findById(id);
        if (!book.isPresent())
            throw BookException.bookNotFound();
        return Converter.convertFromBook(book.get());
    }

    @Override
    public List<BookDto> findByName(String name,Integer limit,Integer offset) {
        try {
            Pageable pageRequest = PageRequest.of(offset, limit);
            return bookRepo.findByNameStartsWith(name,pageRequest).stream()
                    .map(this::convertToDto)
                    .collect(Collectors.toList());
        }catch (Exception e){
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public BookResponse findByPublisherName(String name) {
        User user= userRepo.findByNameStartsWith(name).stream()
                .findFirst()
                .orElseThrow(PublisherException::publisherNotFound);

        List<BookDto> bookDtos= bookRepo.findByPublisher(user).stream()
                .map(this::convertToDto)
                .collect(Collectors.toList());

        return BookResponse.builder()
                .bookDto(bookDtos)
                .build();
    }

    @Override
    public BookDto saveBook(BookReq bookReq) {
        User author=userRepo.findByUsernameAndStatus(bookReq.getAuthorName(),(short)1).orElse(new User());
        if (author==null)
            createAuthor(bookReq.getAuthorName());
        User publisher=userRepo.findByUsernameAndStatus(bookReq.getPublisherName(),(short)1)
                .orElseThrow(PublisherException::publisherNotFound);

        return convertToDto(bookRepo.save(mapToEntity(bookReq,author,publisher)));
    }

    @Override
    public BookDto update(Long id,BookReq bookReq) {
        Book book=bookRepo.findById(id).orElseThrow(BookException::bookNotFound);

        User author=userRepo.findByUsernameAndStatus(bookReq.getAuthorName(),(short)1)
                .orElseThrow(UserException::authorNotFound);
        User publisher=userRepo.findByUsernameAndStatus(bookReq.getPublisherName(),(short)1)
                .orElseThrow(PublisherException::publisherNotFound);
        book.setAuthor(author);
        book.setDescription(bookReq.getDescription());
        book.setName(bookReq.getName());
        book.setPublisher(publisher);
        return convertToDto(bookRepo.save(book));
    }

    @Override
    public BookDto delete(Long id) {
        Book book=bookRepo.findById(id).orElseThrow(BookException::bookNotFound);
        book.setStatus((short) 0);
        return convertToDto(bookRepo.save(book));
    }

    private Book mapToEntity(BookReq bookReq,User author,User publisher){
        return Book.builder().name(bookReq.getName())
                .insertDate(LocalDateTime.now())
                .status((short) 1)
                .author(author)
                .publisher(publisher)
                .description(bookReq.getDescription())
                .name(bookReq.getName())
                .build();
    }

    private BookDto convertToDto(Book book) {
        return BookDto.builder()
                .id(book.getId())
                .name(book.getName())
                .description(book.getDescription())
                .status(book.getStatus())
                .authorName(book.getAuthor().getName())
                .publisherName(book.getPublisher().getName())
                .insertDate(book.getInsertDate())
                .build();
    }
    private User createAuthor(String name){
        return User.builder().userType(new az.unibank.bookStore.entity.UserType(UserType.AUTHOR.getValue(),name))
                .surname(name)
                .name(name)
                .email(null)
                .password(null)
                .insertDate(LocalDateTime.now())
                .status((short) 1)
                .build();
    }
}
