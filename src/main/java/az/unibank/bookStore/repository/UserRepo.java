package az.unibank.bookStore.repository;

import az.unibank.bookStore.entity.Book;
import az.unibank.bookStore.entity.User;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import java.util.List;
import java.util.Optional;

public interface UserRepo extends CrudRepository<User, Long> {

    Optional<User> findByUsernameAndStatus(String username,short status);

    List<User> findByNameStartsWith(String text);


}
