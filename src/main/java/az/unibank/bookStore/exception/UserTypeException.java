package az.unibank.bookStore.exception;

import az.unibank.bookStore.enums.ResultCode;

public class UserTypeException extends BaseException{

    public UserTypeException(ResultCode result) {
        super(result);
    }

    public static UserTypeException userTypeNotFound() {
        return new UserTypeException(ResultCode.USERTYPE_NOT_FOUND);
    }
}
