package az.unibank.bookStore.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class UserDto {
    private Long id;
    private String name;
    private String surname;
    private short status;
    private String email;
    private String username;
    private String password;
    private LocalDateTime insertDate;
    private UserTypeDto userType;
}
