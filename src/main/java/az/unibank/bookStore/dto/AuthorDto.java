package az.unibank.bookStore.dto;

import lombok.Data;

import java.time.LocalDateTime;

@Data
public class AuthorDto {
    private Long id;
    private String name;
    private String surname;
    private String description;
    private short status;
    private LocalDateTime insertDate;
}
