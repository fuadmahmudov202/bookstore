package az.unibank.bookStore.req;

import az.unibank.bookStore.dto.UserTypeDto;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;
import java.time.LocalDateTime;

@NoArgsConstructor
@AllArgsConstructor
@Data
public class UserReq {
    @NotBlank(message = "name can not be null")
    private String name;
    @NotBlank(message = "surname can not be null")
    private String surname;
    @NotBlank(message = "email can not be null")
    private String email;
    @NotBlank(message = "username can not be null")
    private String username;
    @NotBlank(message = "password can not be null")
    private String password;
    @NotBlank(message = "userType can not be null")
    private String userType;

}
