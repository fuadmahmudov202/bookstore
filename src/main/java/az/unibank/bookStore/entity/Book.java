package az.unibank.bookStore.entity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.time.LocalDateTime;

@Data
@Entity
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Table(name = "BOOK")
public class Book {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID", insertable = false)
    private Long id;

    @Column(name = "NAME")
    private String name;

    @Column(name = "STATUS")
    private short status;

    @Column(name = "DESCRIPTION")
    private String description;

    @Column(name = "INSERTDATE")
    private LocalDateTime insertDate;

    @ManyToOne()
    @JoinColumn(name = "publisher_id")
    private User publisher;

    @ManyToOne()
    @JoinColumn(name = "author_id")
    private User author;

}
