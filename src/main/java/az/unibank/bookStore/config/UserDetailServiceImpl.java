package az.unibank.bookStore.config;


import az.unibank.bookStore.entity.User;
import az.unibank.bookStore.exception.LoginException;
import az.unibank.bookStore.repository.UserRepo;
import lombok.RequiredArgsConstructor;
import org.springframework.context.annotation.Primary;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.stereotype.Component;

import java.util.Optional;

@Component
@RequiredArgsConstructor
@Primary
public class UserDetailServiceImpl implements UserDetailsService {

    private final UserRepo userRepo;

    @Override
    public UserDetails loadUserByUsername(String username) {

        User userLogin=userRepo.findByUsernameAndStatus(username,(short) 1)
                .orElseThrow(LoginException::userNotFound);
        return new UserDetailsImpl(userLogin);
    }
}
