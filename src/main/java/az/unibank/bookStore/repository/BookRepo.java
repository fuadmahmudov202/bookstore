package az.unibank.bookStore.repository;

import az.unibank.bookStore.entity.Book;
import az.unibank.bookStore.entity.User;
import org.springframework.data.repository.CrudRepository;

import org.springframework.data.domain.Pageable;import java.util.List;
import java.util.Optional;

public interface BookRepo extends CrudRepository<Book, Long> {

    @Override
    Optional<Book> findById(Long id);

    @Override
    List<Book> findAll();

    List<Book> findByNameStartsWith(String text, Pageable pageable);

    List<Book> findByPublisher(User user);
}
