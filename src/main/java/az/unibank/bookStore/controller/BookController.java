package az.unibank.bookStore.controller;

import az.unibank.bookStore.dto.BookDto;
import az.unibank.bookStore.dto.BookResponse;
import az.unibank.bookStore.dto.UserDto;
import az.unibank.bookStore.req.BookReq;
import az.unibank.bookStore.req.UserReq;
import az.unibank.bookStore.service.BookService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@Slf4j
@RestController
@RequiredArgsConstructor
@RequestMapping("/books")
public class BookController {

    private final BookService bookService;

    @GetMapping("")
    public BookResponse findAll() {
        log.info("endpoint called: / , method: GET");
        return bookService.findAll();
    }

    @GetMapping("/{id}")
    public BookDto getBookById(@PathVariable Long id) {
        log.info("endpoint called: /id/{} , method: GET", id);
        return bookService.findById(id);
    }


    @GetMapping("/bookName/{name}")
    public BookResponse getBookByName(@PathVariable String  name,@PathVariable Integer limit, @PathVariable Integer offset) {
        log.info("endpoint called: bookName/name {}, method: GET",name);
        return new BookResponse(bookService.findByName(name,limit,offset));
    }

    @GetMapping("/publisher/{name}")
    public BookResponse getBookByPublisherName(@PathVariable String  name) {
        log.info("endpoint called: publisher/name {}, method: GET",name);
        return bookService.findByPublisherName(name);
    }

    @PostMapping("/save")
    public BookDto saveNewBook(@Valid @RequestBody BookReq bookReq) {
        log.info("endpoint called: /save , method: GET");
            return bookService.saveBook(bookReq);
    }

    @PutMapping("/{id}")
    public BookDto update(@PathVariable("id") Long id,@Valid @RequestBody BookReq bookReq){
        log.info("endpoint called: /products/{} {} , method: PUT",id,bookReq);
        return bookService.update(id,bookReq);
    }

    @DeleteMapping("delete/{id}")
    public BookDto delete(@PathVariable("id") Long id){
        log.info("endpoint called: /delete/{} {} , method: PUT",id);
        return bookService.delete(id);
    }
}
